from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token
from typing import List #Optional

class HttpError(BaseModel):
    error_message: str

class MoviesOut(BaseModel):
    id: int
    original_title: str
    overview: str
    poster_path : str | None=None
    vote_average : int

class MovieList(BaseModel):
    movies: List[MoviesOut]
