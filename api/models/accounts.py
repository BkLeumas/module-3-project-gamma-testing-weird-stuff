from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token

class AccountForm(BaseModel):
    username: str
    password: str

class AccountIn(BaseModel):
    username: str
    email: str
    password: str

class AccountOut(BaseModel):
    username: str
    id: str
    email: str

class AccountOutWithHashedPassword(AccountOut):
    hashed_password: str

class AccountToken(Token):
    account: AccountOut

class DuplicateAccountError(ValueError):
    path: str
