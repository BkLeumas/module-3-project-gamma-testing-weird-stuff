from fastapi import APIRouter, Depends
from models.movies import MoviesOut, MovieList
from queries.movie_queries import MovieQueries


router = APIRouter()


@router.get('/api/movies/popular/', response_model=MovieList) # , response_model=MoviesOut
def get_popular_movies(
    repo:MovieQueries = Depends()):
        return {
            # repo.get_all()
            'movies': repo.get_all()
        }


@router.get('/api/movies/search/', response_model=MovieList) # , response_model=MoviesOut
def get_searched_movies(
    repo:MovieQueries = Depends()):
        return {
            # repo.get_all()
            'movies': repo.get_by_search("evil")
        }


@router.get('/api/movies/{movie_id}')
def get_movie_details(
        movie_id: str,
        repo:MovieQueries = Depends(),
):
    return {
        'movies': repo.get_by_id(movie_id)
    }
