from fastapi import APIRouter, Depends
from authenticator import authenticator
from models.favorites import FavoriteOut, FavoriteIn
from queries.favorites import FavoritesQueries
from typing import List

router = APIRouter()


@router.post('/api/favorites/', response_model=FavoriteOut)
def add_favorite(
    favorite_in: FavoriteIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: FavoritesQueries = Depends(),
):
    return queries.create(favorite_in, user_id=account_data["id"])


@router.get('/api/favorites/', response_model=List[FavoriteOut])
def get_all_favorites(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: FavoritesQueries = Depends(),
):
    return queries.get_all(user_id=account_data["id"])

@router.delete('/api/favorites/{movie_id}')
def delete_favorite(
    movie_id: str,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: FavoritesQueries = Depends(),
):
    return queries.delete(movie_id, user_id=account_data["id"])
