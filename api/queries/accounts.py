from pydantic import BaseModel
from queries.client import MongoRepo
from models.accounts import AccountIn, DuplicateAccountError, AccountOutWithHashedPassword

class DuplicateAccountError(ValueError):
    pass

class AccountRepo(MongoRepo):
    collection_name = "accounts"

    def create_account(self, info: AccountIn, hashed_password:str):
        account = info.dict()
        if self.get_account_by_email(account["email"]):
            raise DuplicateAccountError
        account["hashed_password"] = hashed_password
        del account["password"]
        response = self.collection.insert_one(account)
        if response.inserted_id:
            account["id"] = str(response.inserted_id)
        return AccountOutWithHashedPassword(**account)

    def get_account_by_email(self, email:str):
        result = self.collection.find_one({"email": email})
        if result is None:
            return None
        result["id"] = str(result["_id"])
        return AccountOutWithHashedPassword(**result)
