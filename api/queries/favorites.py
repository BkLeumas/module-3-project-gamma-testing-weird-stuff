from models.favorites import FavoriteIn, FavoriteOut
from queries.client import MongoRepo


class FavoritesQueries(MongoRepo):
    collection_name = 'favorites'

    def create(self, favorite_in: FavoriteIn, user_id: str):
        favorite = favorite_in.dict()
        favorite["user_id"] = user_id
        search = self.find_one(user_id, favorite_in.movie_id)
        if search:
            return search
        result = self.collection.insert_one(favorite)
        if result.inserted_id:
            favorite["id"] = str(result.inserted_id)
            return favorite

    def find_one(self, user_id: str, movie_id: str):
        result = self.collection.find_one({"user_id": user_id, "movie_id": movie_id})
        if result is not None:
            result["id"] = str(result["_id"])
            del result["_id"]
            return FavoriteOut(**result)
        else:
            return None

    def get_all(self, user_id: str):
        results = self.collection.find({"user_id": user_id})
        return [FavoriteOut(**{**result, "id": str(result["_id"])}) for result in results]

    def delete(self, movie_id: str, user_id: str):
        result = self.collection.delete_one({"user_id": user_id, "movie_id": movie_id})
        if result.deleted_count > 0:
            return True
        else:
            return False
