import os
from pymongo import MongoClient

DATABASE_URL = os.environ.get('DATABASE_URL', '')
WAIT_HOSTS = os.environ.get('WAIT_HOSTS', '')
DATABASE_NAME = os.environ.get('DATABASE_NAME', '')

client = MongoClient(DATABASE_URL)
db = client[DATABASE_NAME]

class MongoRepo:
    @property
    def collection(self):
        return db[self.collection_name]
