import requests
from keys import api_key
from queries.client import MongoRepo
from models.movies import MoviesOut
from typing import List

class MovieQueries(MongoRepo):
    collection_name ='movies'

    # def get_all(self) -> List[MoviesOut]:
    #     url = f'https://api.themoviedb.org/3/movie/popular?language=en-US'
    #     headers = {
    #         "accept": "application/json",
    #         "Authorization": f"Bearer {api_key}"
    #     }
    #     response = requests.get(url, headers=headers).json()
    #     movies = []
    #     print(response)
    #     for num in range(0,5):
    #         movie_data = {
    #             "id": response['results'][num]["id"],
    #             "original_title": response['results'][num]["original_title"],
    #             "overview": response['results'][num]["overview"],
    #             "poster_path": response['results'][num]["poster_path"],
    #             "vote_average": response['results'][num]["vote_average"]
    #         }
    #         movies.append(MoviesOut(**movie_data))
    #         self.collection.insert_one(movie_data)
    #     return movies


    def get_all(self):
        url = f'https://api.themoviedb.org/3/movie/popular?language=en-US'
        headers = {
            "accept": "application/json",
            "Authorization": f"Bearer {api_key}"
        }
        response = requests.get(url, headers=headers).json()
        movies = []
        # print(response)
        print("***** TITLE", response['results'][0]["original_title"])
        for num in range(0,20):
            movie_data = {
                "id": response['results'][num]["id"],
                "original_title": response['results'][num]["original_title"],
                "overview": response['results'][num]["overview"],
                "poster_path": response['results'][num]["poster_path"],
                "vote_average": response['results'][num]["vote_average"]
            }
            # movies.append(MoviesOut(movie_data))
            movies.append(movie_data)
            # self.collection.insert_one(movie_data)
        return movies


    def get_by_search(self, keyword:str):
        movies = []
        # print(response)
        # print("***** TITLE", response['results'][0]["original_title"])
        for pg in range(1,6):
            url = f"https://api.themoviedb.org/3/search/movie?query={keyword}&include_adult=false&language=en-US&page={pg}"
            headers = {
                "accept": "application/json",
                "Authorization": f"Bearer {api_key}"
            }
            response = requests.get(url, headers=headers).json()
            for num in range(0,20):
                movie_data = {
                    "id": response['results'][num]["id"],
                    "original_title": response['results'][num]["original_title"],
                    "overview": response['results'][num]["overview"],
                    "poster_path": response['results'][num]["poster_path"],
                    "vote_average": response['results'][num]["vote_average"]
                }
                # movies.append(MoviesOut(movie_data))
                movies.append(movie_data)
                # self.collection.insert_one(movie_data)
        return movies

# import requests
# from keys import api_key
# from queries.client import MongoRepo


# class MovieQueries(MongoRepo):
#     collection_name ='movies'


#     def get_all(self, page: int):
#         url = f'https://api.themoviedb.org/3/movie/popular?language=en-US&page={page}'
#         headers = {
#             "accept": "application/json",
#             "Authorization": f"Bearer {api_key}"
#         }
#         response = requests.get(url, headers=headers).json()
#         data = {}
#         for num in range(0,20):
#             data["id"] = response['results'][num]["id"]
#             data["original_title"] = response['results'][num]["original_title"]
#             data["overview"] = response['results'][num]["overview"]
#             data["poster_path"] = response['results'][num]["poster_path"]
#             data["vote_average"] = response['results'][num]["vote_average"]
#         movies = []
#         for movie in self.collection.find():
#             movie["id"] = str(movie["_id"])
#             movies.append(data)
# self
        # return movies

    def get_by_id(self, movie_id: str):
        url = f'https://api.themoviedb.org/3/movie/{movie_id}'
        headers = {
            "accept": "application/json",
            "Authorization": f"Bearer {api_key}"
        }
        response = requests.get(url, headers=headers)
        par = response.json()

        return response.json()
